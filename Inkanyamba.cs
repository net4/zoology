﻿using System;

using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Inkanyamba : Animal, ISwimmer
    {
        // Set class attritbutes that are needed , and some that can be set to default
        public Inkanyamba(string name, double age, double hp = 70, double damage = 30, double speed = 1.5)
        {
            Name = name;
            Age = age;
            Hp = hp;
            Damage = damage;
            Speed = speed;
            //adds the object to allAnimals list
            allAnimals.Add(this);
        }

        public override void About()
        {
            Console.WriteLine("The Inkanyamba is a legendary serpent said to be living in a waterfall " +
                "lake area in the northern forests near Pietermaritzburg most commonly in the base " +
                "of Howick Falls, South Africa. The Zulu tribes of the area believe it to be a large " +
                "serpent with an equine head. Most active in the summer months, it is believed that " +
                "the Inkanyamba's anger causes the seasonal storms");
        }
        public void Swim()
        {
            Console.WriteLine($"{Name} enjoys his time in the lake and swims around!");
        }
    }
}
