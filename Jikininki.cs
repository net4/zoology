﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Jikininki : Animal, IFlyer
    {
        // Set class attritbutes that are needed , and some that can be set to default. 
        public Jikininki(string name, double age, double hp = 120, double damage = 10, double speed = 1.2)
        {
            Name = name;
            Age = age;
            Hp = hp;
            Damage = damage;
            Speed = speed;
            //adds the object to allAnimals list
            allAnimals.Add(this);
        }
        public override void About()
        {
            Console.WriteLine("The Jikininki appear as corpse-eating spirits. In Japanese Buddhism, jikininki , " +
                "are the spirits of greedy, selfish " +
                "or impious individuals who are cursed after death to seek out and eat human corpses.");
        }

        public void Fly()
        {
            Console.WriteLine($"{Name} flyes around looking for corpses...");
        }
    }
}
