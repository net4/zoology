﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Ghoul : Animal, IMetamorphose
    {
        // Set class attritbutes that are needed , and some that can be set to default.
        public Ghoul(string name, double age, double hp = 100, double damage = 15, double speed = 1.5)
        {
            Name = name;
            Age = age;
            Hp = hp;
            Damage = damage;
            Speed = speed;

            //adds the object to allAnimals list
            allAnimals.Add(this);
        }

        public override void About()
        {
            Console.WriteLine("The Ghoul is a demon-like being or monstrous " +
                "humanoid originating in pre-Islamic Arabian religion, associated " +
                "with graveyards and consuming human flesh. In modern fiction, the term " +
                "has often been used for a certain kind of undead monster.");
        }
        public void Metamorphose()
        {
            Console.WriteLine($"{Name} morphes into the shape of a wolf. Be careful.");
        }


    }
}
