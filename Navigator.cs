﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Navigator
    {
        public Navigator() { }
        public static void Start()
        {
            Navigator.Introduction();
            Navigator.SearchAndFilter();

        }
        public static void Introduction()
        {
            Console.WriteLine("|-----------------|");
            Console.WriteLine("|---Zoology 4.0---|");
            Console.WriteLine("|-----------------|");

            while (true)
            {
                try
                {
                    int choice;
                    Console.WriteLine("Press 1 to create collection of Animals");
                    Console.WriteLine("Press 2 to use premade collection of Animals");
                    Console.WriteLine("Press 5 to exit application");

                    choice = Convert.ToInt32(Console.ReadLine());

                    if (choice == 1)
                    {
                        int howMany;
                        Console.WriteLine("Create your own collection!!");
                        Console.WriteLine("How many animals do you want to create?");
                        howMany = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < howMany; i++)
                        {
                            Animal.CreateNew();
                            Console.WriteLine($"{ Animal.allAnimals[i].Name} has been created!");
                        }
                        break;
                    }
                    else if (choice == 2)
                    {
                        Animal.Generator();
                        Console.WriteLine("A premade collection of Animals has been created!");
                        break;
                    }else if (choice == 5)
                    {
                        Console.WriteLine("Application ended.");
                        break;
                    }else
                    {
                        Console.WriteLine("Not a valid choice.");
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine("Thats just wrong and you know it...");
                }
               
            }
        }

        public static void SearchAndFilter()
        {
            Console.WriteLine("Now you can filter and sort animals!");
            Console.WriteLine("Press 1 to filter | Press 2 to sort");
            int choice = Convert.ToInt32(Console.ReadLine());
            if (choice == 1)
            {
                Console.WriteLine("How would you like to filter???");
            }else if(choice == 2)
            {
                Animal.SortBy();
            }
        }


    }
}
