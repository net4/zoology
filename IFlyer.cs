﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    interface IFlyer
    {
        void Fly();
    }
}
