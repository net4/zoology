﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Zoology
{   
    //Abstract class to work as a template for sub classes.
    public abstract class Animal 
    {
        public static List<Animal> allAnimals = new List<Animal>(); 

        // Sets variables
        public string Name { get; set; }
        public double Age { get; set; }
        public double Hp { get; set; }
        public double Damage { get; set; }
        public double Speed { get; set; }


        // Method to make your creature dance!
        public void Dance()
        {
            Console.WriteLine($"{Name} impresses everyone with his dance moves!");
        }
        // Method to allow one creature to attack another, and then reduce Hp
        public void Attack(Animal animal)
        {
            animal.Hp -= Damage*Speed;
            Console.WriteLine($"{Name} has attacked {animal.Name} and did {Damage*Speed} damage!");
        }
        // Method to learn more about the creatures mythical history
        public abstract void About();
        // Method to print out all created animals
        public static void viewAll()
        {
            foreach (Animal animal in allAnimals)
            {
                Console.WriteLine("----------");
                Console.WriteLine("Name: " +animal.Name);
                Console.WriteLine("HP: " +animal.Hp);
                Console.WriteLine("Damage: " +animal.Damage);
                Console.WriteLine("Speed: " + animal.Speed);
            }
        }
        //Method to let the a new object be created from user input
        public static void CreateNew()
        {
            try
            {
                Console.WriteLine("You are now creating a new animal!");
                Console.Write("Name: ");
                string name = Console.ReadLine();
                Console.Write("HP: ");
                int hp = Convert.ToInt32(Console.ReadLine());
                Console.Write("Damage: ");
                int damage = Convert.ToInt32(Console.ReadLine());
                Console.Write("Speed: ");
                double speed = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine($"What type of animal is {name}?");
                Console.WriteLine("Press 1 to make Ghoul | Press 2 to make Inkanyamba | Press 3 to make Jikininki");

                int choice;
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        new Ghoul(name, hp, damage, speed);
                        break;
                    case 2:
                        new Inkanyamba(name, hp, damage, speed);
                        break;
                    case 3:
                        new Jikininki(name, hp, damage, speed);
                        break;
                    default:
                        Console.WriteLine("Please chose valid option.");
                        CreateNew();
                        break;

                }
            }
            catch (Exception)
            {
                Console.WriteLine("Please chose valid option.");
                CreateNew();
            }

        }
        //Method to add premade objects to the created animal list
        public static void Generator()
        {
            Ghoul ghoulObj = new Ghoul("Krakkin", 145, 340);
            Jikininki jikininkiObj = new Jikininki("Tashuni", 78);
            Inkanyamba inkanyambaObj = new Inkanyamba("Deany", 30, 1000);
            Ghoul ghoulObj2 = new Ghoul("Brakar", 120, 200);
            Jikininki jikininkiObj2 = new Jikininki("Jankankin", 100);
            Inkanyamba inkanyambaObj2 = new Inkanyamba("Rashad", 50, 800);
        }

        public static void SortBy()
        {
            Console.WriteLine("How would you like to sort thru the animals? From high to low.");
            Console.WriteLine("Press 1 for Age | |Press 2 for HP | |Press 3 for Damage | Press 4 for Speed");
            int choice = Convert.ToInt32(Console.ReadLine());

            var sortedAnimals = from animal in allAnimals
                                orderby (choice == 1 ? animal.Age : (choice == 2 ? animal.Hp : (choice == 3 ? animal.Damage : animal.Speed )))  descending
                                select animal;


            foreach (var animal in sortedAnimals)
            {
                Console.WriteLine("----------");
                Console.WriteLine($"Name : {animal.Name}");
                Console.WriteLine($"Age : {animal.Age}");
                Console.WriteLine($"Hp : {animal.Hp}");
                Console.WriteLine($"Damage : {animal.Damage}");
                Console.WriteLine($"Speed : {animal.Speed}");
            }
        }
    }
}
